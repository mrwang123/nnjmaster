$(document).ready(function() {
    
    
//        // Setup - add a text input to each footer cell
//        $('#productsTable thead ').each( function () {
//            var title = $(this).text();
//            $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
//        } );
    
        var table = $("#productsTable").dataTable({
	processing: true,
        serverSide: true,
        searching : true , 
        select: true,
        iDisplayLength: 10,
        ajax: {
            "url": "TestDataTables/dataTable",
            "type": "POST"
        } ,
//        initComplete: function() {
//            $('#productsTable_filter input').unbind();
//            $('#productsTable_filter input').bind('keyup', function(e) {
//                if(e.keyCode == 13) {
//                    table.fnFilter(this.value);
//                }
//            });
//        },
        autoWidth: false ,
        columnDefs: [
//             {
//            targets: [5],
//            data: null,
//            defaultContent: "<button>Click!</button>"
//            },
            {
                "targets": [ 0 ],
                "visible": true,
                "searchable": true
            },
            {
                "targets": [ 1 ],
                "visible": true , 
                "searchable": true
            }
        ] ,  
        columns: [
        	{ data: "code" },
        	{ data: "product_name" },
        	{ data: "price" },
        	{ data: "min_price_online" },
        	{ data: "maker" }  
        ] , 
	}).dataTableSearch(500) ;
        
//        $('#productsTable tbody').on( 'click', 'button', function () {
//            var data = table.row(  $(this).parents('tr') ).data();
//            alert( "clicked code: " + data['code']  );
//        } );
     
     
        $('#productsTable tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
                
                var data = table.row(  $(this)  ).data();
                alert( "clicked code: " + data['code']  );
            }

        } );
        
        $('#search_input').on( 'keyup', function () {
            table.DataTable().search( this.value ).draw();
        } );
        
        // buttton callback 
        $("#search_global").click(function(){
            var input = document.getElementById("search_input");

            table.DataTable()
                            .columns(1)
                            .search( input.value  )
                            .draw();
            
        });

});


