$(document).ready(function() {
	//wait till the page is fully loaded before loading table
	//dataTableSearch() is optional.  It is a jQuery plugin that looks for input fields in the thead to bind to the table searching
        var table = $("#productsTable").dataTable({
//        var table = $("#productsTable").dataTable({
	processing: true,
        serverSide: true,
        ajax: {
            "url": "products_dataTable",
            "type": "POST"
        },
        autoWidth: false ,
        columnDefs: [
            {
                "targets": [ 3 ],
                "visible": true,
                "searchable": false
            },
            {
                "targets": [ 4 ],
                "visible": true
            }
        ] ,  
        columns: [
                { data: "supply_company" } ,
        	{ data: "code" },
        	{ data: "product_name" },
        	{ data: "price" },
        	{ data: "min_price_online" },
        	{ data: "maker" } 
                
        ] , 
	}).dataTableSearch(500) ;

        
        
    $('#productsTable tbody').on('click', 'tr', function () {
        var data = table.DataTable().row( this ).data() ; 
        alert( 'You clicked on '+data['code']+'\'s row '  );
    } );

//        oTable.$('tbody').click( function () {
//            var data = oTable.fnGetData( this );
//            // ... do something with the array / object of data for the row
//             alert( 'You clicked on '+data[0]+'\'s row' );
//        } );
            
//    $('#productsTable tbody').on('click', 'tr', function () {
//        var id = this.id;
//        var index = $.inArray(id, selected);
// 
//        if ( index === -1 ) {
//            selected.push( id );
//        } else {
//            selected.splice( index, 1 );
//        }
// 
//        $(this).toggleClass('selected');
//    } );

});
