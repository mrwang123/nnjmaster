$(function() {
	//wait till the page is fully loaded before loading table
	//dataTableSearch() is optional.  It is a jQuery plugin that looks for input fields in the thead to bind to the table searching
	$("#supplierInfoTable").dataTable({
	processing: true,
        serverSide: true,
        ajax: {
            "url": "supplier_info_dataTable",
            "type": "POST"
        },
        columns: [
        	{ data: "code" },
        	{ data: "name" },
        	{ data: "admin_name" },
        	{ data: "admin_phone" },
        	{ data: "admin_email" }
        ]
	}).dataTableSearch(500);
});
