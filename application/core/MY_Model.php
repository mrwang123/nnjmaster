<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class My_model extends CI_Model {

	function __construct()
	{
		parent::__construct();
		$this->nnj_db  = $this->load->database('nnj_db', TRUE);
        $this->db      = $this->load->database('nnj_db', TRUE);

	}
}

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */
