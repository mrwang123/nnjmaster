<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	protected $header_data;
	protected $footer_data;
	public $data;
	public $url;

	private $script;

	function __construct()
	{
		parent::__construct();

		self::default_meta_data();
		date_default_timezone_set('Asia/Seoul');
	}

	function default_meta_data(){
		$this->header_data = array();
		$this->header_data['title'] = '엔엔제이 유통 관리 시스템';
		$this->header_data['title_suffix'] = '';
		$this->header_data['description'] = '';
		$this->header_data['keyword'] = '';
		$this->header_data['current_url'] = current_url();

		$this->header_data['css'] = array('css'=> array(
			'main'
		));
	}

	function content_view($view_name)
	{
      
		$this->load->view('templates/_header', $this->header_data );
        $this->load->view( $view_name , $this->data);
		$this->load->view('templates/_footer', $this->footer_data );
//		$this->load->view('layout/local_script', $this->script);
        
	}
    
    function main_view($view_name , $vars = array() )
	{

//		$this->header_data['navi'] = $this->_make_navigation();
		$this->load->view('templates/_header', $this->header_data );
        $this->load->view('main/_left_menu' , $this->data);
        $this->load->view( $view_name , $this->data);
		$this->load->view('templates/_footer', $this->footer_data );
//		$this->load->view('layout/local_script', $this->script);
        
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/controllers/MY_Controller.php */
