<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_Model extends CI_Model implements DatatableModel{

	public function appendToSelectStr() {
        return NULL;
    }

    public function fromTableStr() {
        return 'products';
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }
}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */
