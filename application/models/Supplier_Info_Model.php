<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Supplier_Info_Model extends CI_Model implements DatatableModel{

	public function appendToSelectStr() {
        return NULL;
    }

    public function fromTableStr() {
        return 'supplier_info';
    }

    public function joinArray(){
        return NULL;
    }

    public function whereClauseArray(){
        return NULL;
    }
}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */
