<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Userinfo_Model extends MY_Model {

	function __construct()
	{
		parent::__construct();
	}
    
    
    function check_login($us_id, $us_pass)
    {
        $query = "SELECT * FROM admin_userinfo WHERE admin_id = '".$us_id."' AND password = '".$us_pass."' ";
        $rs = $this->nnj_db->query($query);
        if ($rs->num_rows() > 0) {
            return $rs->row();
        }

        return NULL;
    }

	function checkid($admin_id) {
		$count = $this->nnj_db->where('admin_id', $admin_id)->count_all_results('admin_userinfo');

		if($count > 0) {
			return TRUE;
		}
		return FALSE;
	}

	function checkAuth ($admin_id, $admin_pass) {
//		$stmt = $this->nnj_db
//					->where('admin_id', $admin_id)
//					->where("`password` = PASSWORD('".$admin_pass."')")
//					->get('admin_userinfo');
        
        $stmt = $this->nnj_db
					->where('admin_id', $admin_id)
					->where('password', $admin_pass )
					->get('admin_userinfo');

		return $stmt ? $stmt->first_row('array') : array();
	}

	function getAdminCount() {
		return $this->nnj_db->count_all_results('admin_userinfo');
	}

	function getAdmin($options) {
		$settings = array(
			'limit' => 0,
			'start' => 0
		);
		$settings = array_merge($settings, $options);

		$start = $settings['start'];
		$limit = $settings['limit'];

		$this->nnj_db->from('admin_userinfo')
				->order_by('id', 'desc');

		if ($limit > 0) {
			$this->nnj_db->limit($limit, $start);
		}
		$stmt = $this->nnj_db->get();

		return $stmt->result_array();

	}

	function getAdminInfo($id) {
		$stmt = $this->nnj_db
						->where('id', $id)
						->get('admin_userinfo');
		return $stmt->first_row('array');
	}
    
    function getAdminInfoByID($admin_id) {
		$stmt = $this->nnj_db
						->where('admin_id', $admin_id)
						->get('admin_userinfo');
		return $stmt->first_row('array');
	}

	
}

/* End of file admin_model.php */
/* Location: ./application/models/admin_model.php */
