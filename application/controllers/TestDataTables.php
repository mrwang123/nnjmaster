<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestDataTables extends MY_Controller {

   	function __construct() {
		parent::__construct();
        
        $this->load->model(array( "Admin_Userinfo_Model" ));
        
        // global
		$this->session_us_id     = $this->session->userdata("us_id");
		$this->session_us_level  = $this->session->userdata("us_level");
        
        // session check
		if ($this->session_us_id == NULL) {
			redirect("welcome");
		}
	}
    
    public function index()
    {
        $this->header_data['jsArray'] = array('static/js/jQuery.dtplugin.js','static/js/datatable.test.js');
        $this->header_data['cssArray'] = array();
        
                
        $this->main_view("test_datatable_view") ; 
    }
    
    public function dataTable() {
        
		$this -> load -> library('Datatable', array('model' => 'Products_Model', 'rowIdCol' => 'code'));
		
        $this -> datatable -> setProtectIdentifiers(TRUE) ; 
		$jsonArray = $this -> datatable -> datatableJson( "" );
        
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
		$this -> output -> set_content_type('application/json') -> set_output( json_encode($jsonArray) );

	}
}

