<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

   	function __construct() {
		parent::__construct();
        
        $this->load->model(array( "Admin_Userinfo_Model" ));
        
        $this->load->helper(array('form'));

        $this->load->library(array("form_validation", "session"));

	}
    
    public function index()
    {
        /* Set validation rule for name field in the form */ 
        $login_rules = array(
            array(
                "field" => "us_id", 
                "label" => "아이디를", 
                "rules" => "trim|required"
            ),
            array(
                "field" => "us_pass", 
                "label" => "비밀번호를", 
                "rules" => "trim|required|md5"
            )
        );
        $this->set_form_validation($login_rules);

        if ($this->form_validation->run() == FALSE) { 

            $this->load->view('myform'); 
            
//            $err_msg = validation_errors() ; 
//            alert( $err_msg , "welcome" );

        } 
        else { 

			$this->login_check($_POST);

        } 

    }
    
    
    public function set_form_validation($config)
	{
		// 에러문구 관련 정의
		$this->form_validation->set_error_delimiters("<font color=red>", "</font>");
		$this->form_validation->set_message("required", " <b>!</b>%s 입력해주세요.");
		$this->form_validation->set_message("alpha_dash", "<b>!</b>알파벳,숫자,_,- 만 사용 가능합니다.");
		$this->form_validation->set_message("min_length", "<b>!</b>길이는 4~16자리 이내만 가능합니다.");
		$this->form_validation->set_message("max_length", "<b>!</b>길이는 4~16자리 이내만 가능합니다.");
		$this->form_validation->set_message("numeric", "<b>!</b>숫자만 입력 해주세요.");
		$this->form_validation->set_message("is_unique", "<b>!</b>필드명이 중복됩니다. 다른 값으로 입력해주세요.");
		$this->form_validation->set_message("valid_email", "<b>!</b>이메일 형식이 올바르지 않습니다.");
		$this->form_validation->set_message("matches", "<b>!</b>비밀번호가 일치하지 않습니다.");

		$this->form_validation->set_rules($config);
	}
    
    public function login_check($array)
	{
        $user = array();
		if ( $user = $this->Admin_Userinfo_Model->check_login($array['us_id'], $array['us_pass'])  ) {
			$this->session->set_userdata( array("us_id" => $user->admin_id, "us_level" => $user->level) ); // session
			redirect("main");
            
//            $this->content_view('main_view'); 

		} else {
			alert("가입된 회원이 아니거나 비밀번호가 틀립니다.", "welcome" );
		}
	}
    

}
