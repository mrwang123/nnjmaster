<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Controller {

   	function __construct() {
		parent::__construct();
        
        $this->load->model(array( "Admin_Userinfo_Model" ));
        
        // global
		$this->session_us_id     = $this->session->userdata("us_id");
		$this->session_us_level  = $this->session->userdata("us_level");
        
        // session check
		if ($this->session_us_id == NULL) {
			redirect("welcome");
		}
	}
    
    public function index()
    {
        $this->main_view("main/main_view") ; 
    }
    
    // 내정보
    public function my_info()
    {
        $this->main_view("main/my_info_view") ; 
    }
    
    // 입점몰
    public function mall_info()
    {
        $this->header_data['jsArray'] = array('static/js/jQuery.dtplugin.js','static/js/datatable.mall_info.js');
        $this->header_data['cssArray'] = array();
        
        $this->main_view("main/mall_info_view") ; 
    }

    // 벤더 
    public function vender_info()
    {
        $this->header_data['jsArray'] = array('static/js/jQuery.dtplugin.js','static/js/datatable.supplier_info.js');
        $this->header_data['cssArray'] = array();
        
        $this->main_view("main/vender_info_view") ; 
    }
    
    // 제품  
    public function products()
    {
        $this->header_data['jsArray'] = array('static/js/jQuery.dtplugin.js','static/js/datatable.products.js');
        $this->header_data['cssArray'] = array();
        
        $this->main_view("main/products_view") ; 
    }
    
    // 정산 
    public function calculate()
    {
        $this->main_view("main/calculate_view") ; 
    }
    
    // 정산 
    public function test_datatables()
    {
        $this->main_view("TestDataTables") ; 
    }

    
    
    
    // mall_info_dataTable
    public function mall_info_dataTable() {
		$this -> load -> library('Datatable', array('model' => 'Mall_Info_Model', 'rowIdCol' => 'code'));
		
		$jsonArray = $this -> datatable -> datatableJson( "" );
        
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
		$this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));

	}
    
    // supplier_info_dataTable
    public function supplier_info_dataTable() {
		$this -> load -> library('Datatable', array('model' => 'Supplier_Info_Model', 'rowIdCol' => 'code'));
		
		$jsonArray = $this -> datatable -> datatableJson( "" );
        
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
		$this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));

	}
    
    // supplier_info_dataTable
    public function products_dataTable() {
		$this -> load -> library('Datatable', array('model' => 'Products_Model', 'rowIdCol' => 'code'));
		
		$jsonArray = $this -> datatable -> datatableJson( "" );
        
		$this -> output -> set_header("Pragma: no-cache");
        $this -> output -> set_header("Cache-Control: no-store, no-cache");
		$this -> output -> set_content_type('application/json') -> set_output(json_encode($jsonArray));

	}
    
}

