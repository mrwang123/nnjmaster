<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

    function __construct() {
    	parent::__construct();
        
        // global
		$this->session_us_id     = $this->session->userdata("us_id");
		$this->session_us_level  = $this->session->userdata("us_level");
	}

	public function index()
	{
		        
        if ($this->session_us_id == NULL) {
			redirect( "welcome/login");
		}
        else {
//        $this->main_view('my_info/info_view') ; 
            redirect('main') ; 
        }
	}
    
    public function login() {
        $this->load->view('templates/_header', $this->header_data );
        $this->load->view( 'welcome_view' , $this->data);
        $this->load->view('templates/_footer', $this->header_data );
    }
    
    
    public function logout() {
    	$this->session->sess_destroy();
		redirect("welcome");
    }
    
    
}
