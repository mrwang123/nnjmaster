<!DOCTYPE html>
<html lang="ko">
<html>
	<head>
		<title><?=$title;?></title>

		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="pragma" content="no-cache">
       	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="<?=$description?>"/>
		<meta name="keyword" content="<?=$keyword?>"/>

        <!-- Bootstrap Core CSS -->
        <link href="/static/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="/static/lib/metisMenu/metisMenu.min.css" rel="stylesheet">

        <!-- sbadmin2 CSS -->
        <link href="/static/lib/sbadmin2/css/sb-admin-2.css" rel="stylesheet">
        
        <!-- Custom Fonts CSS -->
        <link href="/static/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        
         <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <?php $ts = time(); ?>
        

		<link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet" />
                
		<link href="/static/css/zepernick.css" rel="stylesheet" />
		
        <!-- jQuery Version  -->
<!--        <script src="/static/lib/jquery/jquery.js"></script>-->
        
		<script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
		
        <script type="text/javascript" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        
        
		<script type="text/javascript" src="/static/js/zjs.utils.js?ts=<?php echo $ts ?>"></script>
        
		<script type="text/javascript" src="/static/js/datatables.colsearch.js?ts=<?php echo $ts ?>"></script>
        
		<?php
            if (isset($jsArray) && is_array($jsArray)) {
                foreach ($jsArray as $value) {
                    echo '<script type="text/javascript" src="' . base_url(). $value . '?ts=' . $ts . '"></script>' . PHP_EOL;
                }
            }
            if (isset($cssArray)) {
                foreach ($cssArray as $value) {
                    echo '<link href="' . base_url() . $value . '?ts=' . $ts . '" type="text/css" rel="stylesheet" />' . PHP_EOL;
                }
            }
			
		?>
        
        
	</head>
    


        
        