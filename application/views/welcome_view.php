<style type="text/css">
html, body {
	height: 100%;
}

html {
	display: table;
	margin: auto;
}

body {
	display: table-cell;
	vertical-align: middle;
}
</style>

<body>

<?php
$attributes = array('name' => 'myForm');
echo form_open('login', $attributes);
?>
    
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading" style="text-align:center;">
					<h3 class="panel-title"><a href="/">엔엔제이 관리 페이지</a></h3>
				</div>
				<div class="panel-body">
                    <form>
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="ID" name="us_id" value="<?php echo set_value('us_id'); ?>" type="text" autofocus />
								<?php echo form_error("us_id"); ?>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="us_pass" type="password" value="<?php echo set_value('password'); ?>" />
								<?php echo form_error("us_pass"); ?>
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<div class="form-group">
								<!-- Change this to a button or input when using this as a form -->
								<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
							</div>
							
<!--                            <div class="form-group text-center">
								<button type="button" class="btn btn-default" onclick="password_forget();">ID / Password 찾기</button>
							</div>-->
                            
						</fieldset>
					</form>
				</div>
                
			</div>
		</div>
	</div>
	<div style="height:130px;"></div>
</div>
    
<?php echo form_close(); ?>


</body>
