<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php site_url() ?>/index.php/welcome">엔엔제이 관리 페이지</a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
            </a>
            
            <ul class="dropdown-menu dropdown-user">
                <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                </li>
                <li class="divider"></li>
                <li><a href="<?php site_url() ?>/index.php/welcome/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- /.navbar-top-links -->
    
<!--    <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>-->

    
    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
            
                <li>
                    <a href="<?php site_url() ?>/index.php/main/my_info"><i class="fa fa-gear fa-1x fa-fw "></i> 내 정보</a>
                </li>
                
                <li>
                    <a href="<?php site_url() ?>/index.php/main/mall_info"><i class="fa fa-laptop fa-1x fa-fw"></i> 입점몰 정보 </a>
                </li>
                
                <li>
                    <a href="<?php site_url() ?>/index.php/main/vender_info"><i class="fa fa-paper-plane fa-1x fa-fw"></i> 벤더 정보 </a>
                </li>
                     
                <li>
                    <a href="<?php site_url() ?>/index.php/main/products"><i class="fa fa-gift fa-1x fa-fw " aria-hidden="true"></i> 제품 리스트 </a>
                </li>
                <li>
                    <a href="<?php site_url() ?>/index.php/main/calculate"><i class="fa fa-money fa-1x fa-fw" aria-hidden="true"></i> 정산 </a>
                </li>
                
                <li>
                    <a href="<?php site_url() ?>/index.php/TestDataTables"><i class="fa fa-money fa-1x fa-fw" aria-hidden="true"></i> 데이터테이블 테스트 </a>
                </li>
                
<!--                <li>
                    <a href="#"><i class="fa fa-table fa-fw"></i> 게시판<span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse in" id="lmenu_board">
                        <li>
                            <a href="/admin/board_list/notice">공지사항</a>
                        </li>
                        <li>
                            <a href="/admin/board_list/faq">자주 묻는 질문</a>
                        </li>
                        <li>
                            <a href="/admin/board_list/qa">1:1 문의</a>
                        </li>
                    </ul>
                </li>-->

            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>
