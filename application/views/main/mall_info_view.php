
<!-- Page Content -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">입점몰 정보</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->
    
    <div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
              <div class="panel-body">
              		<table id="mallInfoTable">
              			<thead>
              				<tr>
<!--              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>-->
              				</tr>
              				<tr>
              					<th>코드</th>
              					<th>입점몰 이름</th>
              					<th>수수료</th>
              					<th>담당자 이름</th>
              					<th>담당자 전화번호</th>
              					<th>담당자 이메일</th>
              				</tr>
              			</thead>
              			<tbody></tbody>
              		</table>
              </div>
            </div>
	    </div>
	</div>
    
    
    
    
    
    
    
</div>
<!-- /#page-wrapper -->




