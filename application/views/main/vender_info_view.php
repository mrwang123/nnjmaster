
<!-- Page Content -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">벤더 정보</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    
    
    <div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
              <div class="panel-body">
              		<table id="supplierInfoTable">
              			<thead>
              				<tr>
<!--              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>
              					<td><input type="text" /></td>-->
              				</tr>
              				<tr>
              					<th>코드</th>
              					<th>벤더 이름</th>
              					<th>담당자 이름</th>
              					<th>담당자 전화번호</th>
              					<th>담당자 이메일</th>
              				</tr>
              			</thead>
              			<tbody></tbody>
              		</table>
              </div>
            </div>
	    </div>
	</div>
    
    
    
    
    
    
</div>
<!-- /#page-wrapper -->


<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
              <div class="panel-body">
              		<table id="sampleOrderTable">
              			<thead>
              				<tr>
              					<th>Order#</th>
              					<th>Order Dt</th>
              					<th>Status</th>
              					<th>Customer</th>
              					<th>Contact</th>
              					<th>Credit Limit</th>
              				</tr>
              			</thead>
              			<tbody></tbody>
              		</table>
              </div>
            </div>
	    </div>
	</div>

