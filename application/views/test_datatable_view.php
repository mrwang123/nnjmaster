



<!-- Page Content -->
<div id="page-wrapper">
    
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Test DataTable</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    
    <div class="container-fluid">
        
        <div class="row">
			<div class="col-lg-12">
				&nbsp;
			</div>
			<!-- /.col-lg-12 -->
		</div>
        
        <div class="row">
            <div class="col-lg-6">
                <div class="input-group">
<!--                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                  <input type="text" class="form-control" placeholder="Search for...">-->
                </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->

            
            <div class="col-lg-6">
              <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for..." id="search_input">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="button" id="search_global" >찾기</button>
                </span>
              </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
        </div>
        <!-- /.row -->
        
        <div class="row">
			<div class="col-lg-12">
				&nbsp;
			</div>
			<!-- /.col-lg-12 -->
		</div>
        
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  <div class="panel-body">
                        <table id="productsTable">
                            <thead>
                                <tr>
                                    <th>코드</th>
                                    <th>상품명</th>
                                    <th>상품가격</th>
                                    <th>온라인 최저가</th>
                                    <th>제조사</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                  </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- /.container-fluid -->
    
    <div id="push"></div>
</div>
<!-- /#page-wrapper -->







